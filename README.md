# Dumb Bloom Filter and Uniqueness Test

This is a simple implementation of a very obvious bloom filter in python, just
using the built-in python `hash` function for hashing.

Also included is a use of this filter to estimate unique items in an iterable, showing
off how it can be backed by a sparse file as a buffer instead of storing the
bloom filter in memory.

This was inspired by [this article](https://justinjaffray.com/a-charming-algorithm-for-count-distinct/) and [this Reddit thread](https://www.reddit.com/r/programming/comments/10mhkml/a_charming_algorithm_for_countdistinct/j63jz3v/).
