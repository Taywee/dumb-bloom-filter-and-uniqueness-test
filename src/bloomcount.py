from typing import Iterable
from dumb_bloom_filter import BloomFilter

def count_unique(iterable: Iterable, *args, **kwargs) -> int:
    bloom_filter = BloomFilter(*args, **kwargs)

    unique = 0
    accumulated_error = 0.0

    for item in iterable:
        if bloom_filter.add(item):
            unique += 1
        else:
            accumulated_error += bloom_filter.error_probability(unique)
            if accumulated_error >= 1.0:
                unique += 1
                accumulated_error -= 1.0

    return unique

            
