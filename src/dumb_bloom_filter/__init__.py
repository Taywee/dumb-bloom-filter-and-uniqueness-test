from typing import Any, Union
from collections.abc import ByteString, Container, Sized
from typing import Iterable, Tuple, cast
from struct import pack, unpack
from abc import abstractmethod
from math import e

class ByteBuffer(Sized):
    '''An abstract base class for something like a non-resizable non-iterable Sequence of bytes.
    '''
    @abstractmethod
    def __getitem__(self, index: int) -> int:
        raise NotImplementedError

    @abstractmethod
    def __setitem__(self, index: int, byte: int) -> None:
        raise NotImplementedError

    @classmethod
    def __subclasshook__(cls, C):
        if cls is ByteBuffer:
            return issubclass(C, Sized) and all(any(method in B.__dict__ for B in C.__mro__) for method in ('__getitem__', '__setitem__'))

ByteBuffer.register(bytearray)

class BloomFilter(Container):
    '''A simple bloom filter with a customizable buffer and directly changeable k.
    '''

    def __init__(self, buffer: Union[ByteBuffer, int] = 1024 * 128, k: int = 3):
        assert k > 0

        if isinstance(buffer, int):
            buffer = cast(ByteBuffer, bytearray(buffer))

        assert isinstance(buffer, ByteBuffer)

        self.buffer = buffer
        self.__bit_size = len(buffer) * 8
        assert self.__bit_size > 0
        self.__k = k

    def __bit_positions(self, value: Any) -> Iterable[Tuple[int, int]]:
        '''Get the bit positions, as a byte offset and bit offset'''

        for i in range(self.__k):
            bit_offset = hash((i, value)) % self.__bit_size
            byte_offset = bit_offset // 8
            byte_bit_offset = bit_offset % 8
            yield byte_offset, byte_bit_offset

    def add(self, value: Any) -> bool:
        '''Add the item, returning whether the buffer was changed by this operation.
        '''
        added = False
        for byte_offset, bit_offset in self.__bit_positions(value):
            mask = 1 << bit_offset
            byte = self.buffer[byte_offset]
            new_byte = byte | mask

            if byte != new_byte:
                self.buffer[byte_offset] = new_byte
                added = True
        return added

    def error_probability(self, n: int) -> float:
        '''Estimate probability of error for the given k and number of items.'''
        return (1 - e ** (-self.__k * n / self.__bit_size)) ** self.__k

    def __contains__(self, value: Any) -> bool:
        return all(self.buffer[byte_offset] & (1 << bit_offset) for byte_offset, bit_offset in self.__bit_positions(value))

