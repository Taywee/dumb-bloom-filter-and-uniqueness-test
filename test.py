from bloomcount import count_unique
import random
import sqlite3
from pathlib import Path
from contextlib import ExitStack
from struct import pack
from tempfile import TemporaryDirectory

class FileBuffer:
    '''A sparse file as a buffer.
    '''

    def __init__(self, path: Path, size: int):
        self.__path = path
        self.__size = size

    def __enter__(self) -> 'FileBuffer':
        self.__stack = ExitStack().__enter__()
        self.__file = self.__stack.enter_context(self.__path.open('wb+'))
        return self

    def __exit__(self, type, exception, traceback):
        del self.__file
        return self.__stack.__exit__(type, exception, traceback)

    def __getitem__(self, index: int) -> int:
        self.__file.seek(index)
        value = self.__file.read(1)
        if value:
            return value[0]
        else:
            return 0

    def __setitem__(self, index: int, value: int) -> None:
        self.__file.seek(index)
        self.__file.write(pack('B', value))

    def __len__(self) -> int:
        return self.__size

print('generating numbers')
items = [random.randint(0, i) for i in range(1000000)]
print('generated numbers')
true = len(frozenset(items))

print(f'{true=}')

with TemporaryDirectory() as tempdir:
    with FileBuffer(size=1024 * 256, path=Path(tempdir) / 'bloom.raw') as buffer:
        estimate = count_unique(items, buffer=buffer, k=3)
print(f'{estimate=}')


